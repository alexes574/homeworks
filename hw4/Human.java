package HappyFamily;

import java.util.Arrays;

public class Human {
    public String name ;
    public String surname ;
    public int year ;
    public int iq ;
    public Human mother;
    public Human father;
    public String [][] schedule = new String[2][2];
    public Pet pet;

    public Human (String name, String surname, int year){
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human (String name, String surname, int year, Human mother, Human father){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
    }

    public Human (String name, String surname, int year,int iq, Human mother, Human father, String[][] schedule){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq  = iq;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }


    public void greetPet(){
        System.out.printf("\nПривет,%s",pet.nickname);
    }

    public void describePet() {
        if (pet.trickLevel <= 50) {
            System.out.printf("\n У меня есть %s, ему %d лет , он почти не хитрый.", pet.species, pet.age);
        } else {
            System.out.printf("\nУ меня есть %s, ему %d лет , он очень хитрый.", pet.species, pet.age);
        }
      }
      public String toString(){
        return "\nHuman{name ='" + name + "', surname='" + surname + "', year=" + year +", iq=" + iq + ", mother=" + mother.name
             + " " + mother.surname  +  ", father=" + father.name + " " + father.surname +
                ", schedule - " + Arrays.deepToString(schedule) +",pet =" + pet.toString() + "}";
      }


    }