package HappyFamily;

import java.util.Arrays;

public class Pet {
    public String species;
    public String nickname;
    public int age;
    public int trickLevel;
    public String []  habits;

    public Pet(String species,String nickname){
        this.species = species;
        this.nickname = nickname;
    }

    public Pet(String species,String nickname, int age,int trickLevel,String[] habits){
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public void eat() {
        System.out.println("\nЯ кушаю!");
    }

    public void respond(){
        System.out.printf("\nПривет, хозяин. Я - %s . Я соскучился!",nickname);
    }

    public void foul(){
        System.out.println("\nНужно хорошо замести следы...");
    }
    public String toString(){
        return "\n"+species +"{nickname ='" + nickname + "', age =" + age + ", trickLevel =" + trickLevel + ", habits ="
        + Arrays.deepToString(habits) +  "}";
    }



}

